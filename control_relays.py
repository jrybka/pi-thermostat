from gpiozero import LED
from time import sleep

from read_temperature import get_temperature

heat = LED("GPIO17")
cool = LED("GPIO22")
fan = LED("GPIO27")

COOL_TEMP = 25.0
HEAT_TEMP = 23.0

while True:
    current_temperature = get_temperature()
    print(current_temperature)

    if current_temperature > HEAT_TEMP:
        heat.off()
    elif current_temperature < HEAT_TEMP:
        heat.on()

    if current_temperature > COOL_TEMP:
        cool.on()
    elif current_temperature < COOL_TEMP:
        cool.off()

    if heat.is_active or cool.is_active:
        fan.on()
    else:
        fan.off()

    print(f"Heat: {heat.is_active}, Cool: {cool.is_active}, Fan: {fan.is_active}")
    sleep(1)