import os

TEMPERATURE_DEVICE = "28-01206b2ddd90"
DEVICE_PATH = "/sys/bus/w1/devices/{device_id}/temperature"


def get_temperature() -> float:
    with open(DEVICE_PATH.format(device_id=TEMPERATURE_DEVICE)) as temperature_file:
        temperature = temperature_file.read()

    return float(temperature) / 1000.0
